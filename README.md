# README

## Installation
```bash
cd /path/to/project
yarn install
```

## Build demo
```bash
yarn webpack
```

## Use
```js
import escapeHTML from '@casino/escape-html';
let html = '<div>&nbsp</div>';
let result = escapeHTML(html);
console.log(result);
```